const express =require('express');
require ('dotenv/config');

const account=require("./routes/route.routes");
const app =express();

app.use('/',account);
app.listen(process.env.PORT);

const mongoose  = require('mongoose');

mongoose.connect('mongodb://localhost:27017/firstDB',()=>{
    console.log('connected to db ')
})
